// ==UserScript==
// @name         BustABit extractor
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.bustabit.com/play
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        none
// ==/UserScript==

var savedData = new Array();
var currentGame = undefined;

const onGameStarted = () => {

    const gameInstance = window._engine;
    const allBets = Array.from(window._engine.playing.values())

    currentGame = {
        totalPlayers : allBets.length,
        totalBet : allBets.reduce((acc,current) => {return current.wager+acc},0),
        exepectedReturn : allBets.reduce((acc,current) => {return current.wager * current.payout + acc},0),
        allBets : allBets.map(o=>{return {w:o.wager, p:o.payout}}), //compress data
        date: new Date().getTime(),
    }

}

const onGameStop = () => {
    if(currentGame===undefined) return;
    const gameInstance = window._engine;
    currentGame.bust = gameInstance.bust
    console.log("Game ended: "+savedData.length,currentGame)

    savedData.push(currentGame)
}

const injectDataCatcher = () => {
    const originalFunctionStart = window._socket._events.gameStarted.fn
    window._socket._events.gameStarted.fn = () => { originalFunctionStart(); onGameStarted();}

    const originalFunctionStop = window._socket._events.gameEnded.fn
    window._socket._events.gameEnded.fn = (e) => {originalFunctionStop(e); onGameStop(); }
}

const createSaveButton = () => {

    const startDowload = () => {
        console.log("Start download")
        const tempA = document.createElement('a');
        tempA.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(JSON.stringify(savedData)));
        tempA.setAttribute('download', "BustABitExtract.json");
        tempA.style.display = 'none';
        document.body.appendChild(tempA);
        tempA.click();
        document.body.removeChild(tempA);
        console.log("Finished")
    }

    const targetPlace = document.querySelectorAll('nav>div')[0]
    var a = document.createElement('a')
    var linkText = document.createTextNode("Export data");
    a.appendChild(linkText);
    a.onclick = startDowload
    targetPlace.appendChild(a);
}

(function() {
    'use strict';
    window.addEventListener('load', function() {
        createSaveButton()
        injectDataCatcher()
    }, false);
})();

