const tools = require('./tools.js');

function sendChallengeToServer(serverKey, extractedServerKey, challenge, currentConnection) {

    const secret = "" // we are not authenticated so no secret
    const firstPart = challenge.substring(serverKey.length)
    const finalChallenge = `${firstPart}:10:${tools.generateKey()}:${secret}`

    console.log("Send all key challenge to server", finalChallenge)
    currentConnection.sendUTF(finalChallenge)
}

function resolveChallenge(serverKey, currentConnection) {

    const extractedServerKey = 2 * Number(serverKey[0])

    //self is originaly the navigator window frame. We override it to inject our own code and hack the generate function
    self = {
        addEventListener: (type, worker, bool) => {
            worker({
                data: {
                    id: 1,
                    method: "generate",
                    args: [serverKey, extractedServerKey]
                }
            })
        },
        postMessage: (data) => {
            console.log(`Challenge resolved`, data.result)
            sendChallengeToServer(serverKey, extractedServerKey, data.result, currentConnection)
        }
    }

    var hardLoadedJS = new Function(tools.challengeGenerator)
    hardLoadedJS()
}

module.exports.resolveChallenge = resolveChallenge
