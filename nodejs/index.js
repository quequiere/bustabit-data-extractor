const WebSocketClient = require('websocket').client;
const BitView = require('bit-buffer').BitView;
const security = require('./security.js');

const wsAddress = "wss://gs.bustabit.com/ws"

var challengeSent = false;
var currentConnection = undefined;

function onConnect(connection) {

    currentConnection = connection;
    connection.on('error', (error) => { console.log("Connection Error: " + error.toString()) });
    connection.on('close', () => { console.log('Connection Closed') });
    connection.on('message', onMessage);
}


function onMessage(message) {
    if (!challengeSent) {
        challengeSent = true;
        security.resolveChallenge(message.utf8Data, currentConnection)
        return
    }

    if (message.type === "binary") {

        const a = message.binaryData.indexOf(58)
        const mType = message.binaryData.subarray(1, a).toString('utf8')

        const messageData = message.binaryData.subarray(a + 1, message.binaryData.length)


        if (mType === "cashedOut") {
            const t = new Float64Array(new Uint8Array(messageData).buffer)
            const currentScore = t[3]
            // console.log(`[${mType}] ==> ${currentScore}`)
        }
        else if (mType === "gameEnded") {
            const t = new Float64Array(new Uint8Array(messageData).buffer)
            const finalBust = t[6]
            console.log(`[${mType}] ==> ${finalBust}`)
        } else if (mType === "betsPlaced") {
            // bets: _e((t=>e.bets(t)), e.betsLength(), Is.decode)
            const betsQuantity = new Int32Array(new Uint8Array(messageData).buffer)[3]
            const betsQuantity2 = new Int32Array(new Uint8Array(messageData).buffer)[4]
            const betsQuantity3 = new Int32Array(new Uint8Array(messageData).buffer)[5]

            console.log(`[${mType}] ==> `)
        }
        else {
            console.log(`[${mType}]`)
        }
    }

}


var client = new WebSocketClient();
client.connect(wsAddress);
client.on('connectFailed', (f) => console.log(f))
client.on('connect', onConnect);
